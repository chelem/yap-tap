# Documentation: http://docs.brew.sh/Formula-Cookbook.html
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!

class Test < Formula
  desc "Installs the YAP parser"
  homepage "http://www.chelem.co.il"
  url "https://bitbucket.org/chelem/yap-brew/get/test_tag.tar.gz"
  sha256 "aa404381f8c64f664ff0ba3bb9365217502191a93d45c03941749d79067413a3"


  def install
      print "In yap-tap\n"
      result = exec("python pyscript.py")
      bin.install "testscript"
  end
end
